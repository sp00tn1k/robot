#!/usr/bin/env python
import rospy
import numpy as np
from cv_bridge import CvBridge
import cv2
import math
from copy import deepcopy

from std_msgs.msg import Int8, UInt8
from nav_msgs.msg import OccupancyGrid
from sensor_msgs.msg import Image
from geometry_msgs.msg import PoseStamped, Pose, Point, Quaternion, Twist
from move_base_msgs.msg import MoveBaseActionResult
from actionlib_msgs.msg import GoalID
import tf

class Minimap:
  def __init__(self):
    use_robot_pos= rospy.get_param('~use_robot_pos', 'true')
    self.static_map = rospy.get_param('~static_map', 'False')
    self.map_frame = rospy.get_param('~map_frame', 'map')
    self.base_frame = rospy.get_param('~base_frame', 'base_footprint')
    self.update_frequency = rospy.get_param('~update_frequency', 2)

    self.map_locations = rospy.get_param('~locations', False)

    self.bridge = CvBridge()
    self.rgb_grid = np.zeros([1,1,3], np.uint8)
    self.rgb_grid_copy = np.zeros([1,1,3], np.uint8)
    self.initial_map_received = False
    self.map_updated = False
    self.last_position = Point(0,0,0)
    self.map_width = 0
    self.map_height = 0

    rospy.Subscriber("/map", OccupancyGrid, self.map_cb)
    rospy.Subscriber("/move_base_simple/goal", PoseStamped, self.goal_cb)
    rospy.Subscriber("move_base/result", MoveBaseActionResult, self.goal_result_cb)
    rospy.Subscriber("cmd_vel_in", Twist, self.teleop_input_cb)
    self.minimap_pub = rospy.Publisher("minimap", OccupancyGrid, queue_size=10, latch=True)
    self.goal_cancel_pub = rospy.Publisher("move_base/cancel", GoalID, queue_size=10)
    #self.pose_pub = rospy.Publisher("minimap/robot_pose", PoseStamped, queue_size=10, latch=True)

    self.tf_listener = tf.TransformListener()

    self.robot_pose = PoseStamped()
    self.robot_pose.header.frame_id = "map"
    self.goal_msg = PoseStamped()
    self.goal_msg.header.frame_id = "map"
    self.published_map = OccupancyGrid()
    self.current_goal = None
    self.goal_updated = False

  # Update goal to be drawn on minimap
  def goal_cb(self, data):
    if self.current_goal != data:
      self.current_goal = data
      self.goal_updated = True
      rospy.loginfo("New goal received.")

  # If the goal has been reached or aborted, set variable to none
  def goal_result_cb(self, data):
    if self.current_goal:
      if data.status.status == 3 or data.status.status == 4 or (data.status.status == 2 
                                                            and data.status.text == ''):
        self.current_goal = None
        self.goal_updated = True

  # If there is teleop input while a move_base goal is active, cancel that goal
  # TODO: Interrupt goal instead? 
  def teleop_input_cb(self, data):
    if self.current_goal:
      if data.linear.x != 0 or data.angular.z != 0:
        self.goal_cancel_pub.publish(GoalID())
        self.current_goal = None
      

  def map_cb(self, data):
    rospy.logdebug("Received map update.")

    # Only do something if the map is not static or the map has not yet been loaded
    if (not self.static_map) | (not self.initial_map_received):
      self.rgb_grid_copy = np.zeros([data.info.height, data.info.width, 3], np.uint8)
      self.current_map = data
      for i in range(data.info.height):
        for j in range(data.info.width):
          if data.data[i*data.info.width + j] == -1:
            self.rgb_grid_copy[i,data.info.width-j-1,0:3] = (140,140,160)
          else: 
            self.rgb_grid_copy[i,data.info.width-j-1,0:3] = (-data.data[i*data.info.width+ j]+100) * 2.55
      self.rgb_grid = self.rgb_grid_copy.copy() # avoid publishing incompletely updated map
      self.map_updated = True
      self.initial_map_received = True


  def publish_map(self):
    last_pose = ([0, 0, 0], [0, 0, 0, 1])
    while not rospy.is_shutdown():
      if self.initial_map_received:
        try:
          (position, orientation) = self.tf_listener.lookupTransform('/map', 
                                                  self.base_frame, rospy.Time(0))
          # Map robot's position to image coordinates
          coordinate = (self.current_map.info.width - int(round((position[0] 
                       - self.current_map.info.origin.position.x) / self.current_map.info.resolution)), 
                       int(round((position[1] - self.current_map.info.origin.position.y) 
                                                                  / self.current_map.info.resolution)))
          direction_angle = tf.transformations.euler_from_quaternion(orientation)
          second_point = (int(round(coordinate[0]-4*math.cos(direction_angle[2]))), 
                          int(round(coordinate[1]+4*math.sin(direction_angle[2]))))

          # Only update if something changed
          if (np.linalg.norm(np.array([last_pose[0][0]-position[0], last_pose[0][1]-position[1]])) > 0.1)\
                                                | self.map_updated | self.goal_updated:
            # Copy current map for drawing and publishing
            pub_copy = self.rgb_grid.copy()

            # Draw locations, if any  
            if self.map_locations:
              for i in self.map_locations['text']:
                cv2.putText(pub_copy, i, 
                    org=(self.current_map.info.width - int(round((self.map_locations['text'][i][0] 
                          - self.current_map.info.origin.position.x) / self.current_map.info.resolution)) -2,\
                    int(round((self.map_locations['text'][i][1] - self.current_map.info.origin.position.y) 
                    / self.current_map.info.resolution)) -2),\
                    fontFace=cv2.FONT_HERSHEY_SIMPLEX, fontScale=0.4, color=(200,100,150))
                   
            # Draw goal, if any
            if self.current_goal:
              goal_map_pose = (self.current_map.info.width - int(round((self.current_goal.pose.position.x 
                              - self.current_map.info.origin.position.x) / self.current_map.info.resolution)),\
                              int(round((self.current_goal.pose.position.y 
                              - self.current_map.info.origin.position.y) / self.current_map.info.resolution)))
              cv2.circle(pub_copy, center=goal_map_pose, radius=4, color=(100,100,200), thickness=-1)

            # Draw robot
            cv2.circle(pub_copy, center=coordinate, radius=4, color=(100,200,100), thickness=-1) # position
            cv2.circle(pub_copy, center=second_point, radius=2, color=(200,100,100), thickness=-1) # orientation

            self.published_map = deepcopy(self.current_map)
            
            # Publish map image as occypancy grid message
            self.published_map.data = (pub_copy.reshape(-1).astype(int)-128).tolist()
            self.published_map.header.stamp = rospy.Time.now()
            self.minimap_pub.publish(self.published_map)

            # Publish robot pose
            #self.robot_pose.header.stamp = rospy.Time.now()
            #self.robot_pose.pose = Pose(Point(position[0], position[1], position[2]), Quaternion(orientation[0], orientation[1], orientation[2], orientation[3]))
            #self.pose_pub.publish(self.robot_pose)

            last_pose = (position, orientation)
            self.map_updated = False
            self.goal_updated = False
          rospy.sleep(1/self.update_frequency)    

        except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
          rospy.logwarn("Transform between frames %s and %s not found.", self.map_frame, self.base_frame)
          rospy.sleep(1.0)
          continue
      

if __name__ == '__main__':
  try:
    rospy.init_node('minimap')
    mc = Minimap()
    mc.publish_map()
  except rospy.ROSInterruptException:
    pass

