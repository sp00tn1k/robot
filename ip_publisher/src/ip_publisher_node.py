#!/usr/bin/env python
import rospy
import socket
from std_msgs.msg import String


class IpPublisher:

  def __init__(self):

    self.ip_pub = rospy.Publisher("local_ip", String, queue_size=10, latch=True)
    self.ip = String()
    self.updated = False
    self.previous = ''

  def get_ip(self):
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    try:
      s.connect(('10.255.255.255', 1))
      self.ip.data = s.getsockname()[0]
      if self.ip.data != self.previous:
        self.updated = True
        self.previous = self.ip.data
        rospy.loginfo("IP updated")
      return True
    except:
      self.ip.data = '127.0.0.1'
      return False
    finally:
      s.close()

  def publish_ip(self):
    while not rospy.is_shutdown():
      if not self.get_ip():
        rospy.logdebug("Could not resolve IP address.")
      if self.updated:
        self.ip_pub.publish(self.ip)
        self.updated = False

      rospy.sleep(10)


if __name__ == '__main__':
  rospy.init_node('ip_publisher')
  ipb = IpPublisher()
  ipb.publish_ip()
