# IP Publisher

The ROS node publishes the current IP address on a latched topic. Resolves to localhost if no connection is available.

Published topic: 

~<name>/local_ip (std_msgs/String)


TODO: Select specific network device via parameter, if several are available and connected.