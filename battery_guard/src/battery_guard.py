#!/usr/bin/env python
import rospy
import os

from sensor_msgs.msg import BatteryState
from sound_play.msg import SoundRequestGoal, SoundRequest, SoundRequestAction
from actionlib import SimpleActionClient

class BatteryGuard:
    def __init__(self):

        
        self.low_voltage = rospy.get_param('~low_voltage', 21.0)
        self.critical_voltage = rospy.get_param('~critical_voltage', 20.0)
        self.last_measures = []
        for i in range(0,15):
            self.last_measures.append('high')

        rospy.Subscriber("battery", BatteryState, self.battery_cb)

        self.client = SimpleActionClient('sound_play', SoundRequestAction)
        self.client.wait_for_server()
        self.goal = SoundRequestGoal()
        self.low_request = SoundRequest(sound=3, command=1, volume=0.8)
        self.critical_request = SoundRequest(sound=5, command=1, volume=1.0)

    def battery_cb(self, data):
        rospy.logdebug("Callback receiving battery state message")
        del self.last_measures[0]
        if data.voltage < self.critical_voltage:
            self.last_measures.append('critical')
        elif data.voltage < self.low_voltage:
            self.last_measures.append('low')
        else:
            self.last_measures.append('high')

    def check_loop(self):
        while not rospy.is_shutdown():
            #print(self.last_measures)
            if self.last_measures.count('critical') > 10:
                rospy.logwarn("Power below critical threshold of %s!", self.critical_voltage)
                self.goal.sound_request = self.critical_request
                self.client.send_goal(self.goal)
            elif self.last_measures.count('low') > 10:
                rospy.loginfo("Power below low threshold of %s!", self.low_voltage)
                self.goal.sound_request = self.low_request
                self.client.send_goal(self.goal)

            rospy.sleep(20)


        
if __name__ == '__main__':
    try:                                                                                                                               
        rospy.init_node('battery_guard')
        bg = BatteryGuard()
        bg.check_loop()
    
    except rospy.ROSInterruptException:
        pass

