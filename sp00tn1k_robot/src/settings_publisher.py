#!/usr/bin/env python
import rospy

from std_msgs.msg import Int8, UInt8
from diagnostic_msgs.msg import DiagnosticStatus, KeyValue

class SettingsPublisher:

  def __init__(self):
    self.settings = rospy.get_param('~settings', False)

    self.settings_msg = DiagnosticStatus()

    self.settings_pub = rospy.Publisher("client_settings", DiagnosticStatus, queue_size=10, latch=True)

    if self.settings:
      for i in self.settings['params']:
        self.settings_msg.values.append(KeyValue(i, self.settings['params'][i]))
      self.settings_pub.publish(self.settings_msg)
    else:
      rospy.logwarn("No settings file")
          
                
if __name__ == '__main__':
  try:
    rospy.init_node('settings_publisher')
    sp = SettingsPublisher()
    rospy.spin()
  
  except rospy.ROSInterruptException:
    pass

