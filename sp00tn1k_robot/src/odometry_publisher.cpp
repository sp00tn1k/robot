#include <ros/ros.h>
#include <tf/transform_broadcaster.h>
#include <nav_msgs/Odometry.h>
#include <geometry_msgs/Twist.h>
#include <string>

geometry_msgs::Twist inc_twist;

void velCallback(const geometry_msgs::Twist::ConstPtr& msg)
{
	inc_twist = *msg;
}

int main(int argc, char** argv){
	ros::init(argc, argv, "odometry_publisher");

	ros::NodeHandle n;
    ros::NodeHandle np(ros::this_node::getName()); // for reading parameters
    std::string tf_prefix = "";
    np.param<std::string>("tf_prefix", tf_prefix, "");
	ros::Publisher odom_pub = n.advertise<nav_msgs::Odometry>("odom", 50);
	ros::Subscriber velocitysub = n.subscribe("velocity", 100, velCallback);
	tf::TransformBroadcaster odom_broadcaster;

	double x = 0.0;
	double y = 0.0;
	double th = 0.0;

	double vx = 0.0;
	double vy = 0.0;
	double vth = 0.0;

	double dt, delta_x, delta_y, delta_th;

	ros::Time current_time, last_time;
	current_time = ros::Time::now();
	last_time = ros::Time::now();
	
	int last_msgs = 0; // compare to check if there was an incoming twist


	// transform message
	geometry_msgs::TransformStamped odom_trans;
	odom_trans.header.frame_id = tf_prefix + "/odom";
	odom_trans.child_frame_id = tf_prefix + "/base_footprint";
    odom_trans.transform.translation.z = 0.0;

	// odom message
	nav_msgs::Odometry odom;
	odom.header.frame_id = "odom";
	odom.child_frame_id =  "base_footprint";
	odom.pose.pose.position.x = 0.0;
	odom.pose.pose.position.y = 0.0;
	odom.pose.pose.position.z = 0.0;

	geometry_msgs::Quaternion odom_quat;
	
	ros::Rate r(5.0);

	// Loop
	while(n.ok()){

		ros::spinOnce();               // check for incoming messages

		current_time = ros::Time::now();

		// update values from last twist
		vx = inc_twist.linear.x;
		vy = inc_twist.linear.y;
		vth = inc_twist.angular.z;

		dt = (current_time - last_time).toSec();
		delta_x = vx * cos(th) * dt;
		delta_y = vx * sin(th) * dt;
		delta_th = vth * dt;			

		x += delta_x;
		y += delta_y;
		th += delta_th;

		//since all odometry is 6DOF we'll need a quaternion created from yaw
    	odom_quat = tf::createQuaternionMsgFromYaw(th);

		// publish tf transform
		odom_trans.header.stamp = current_time;
		odom_trans.transform.translation.x = x;
       	odom_trans.transform.translation.y = y;
		odom_trans.transform.rotation = odom_quat;
		odom_broadcaster.sendTransform(odom_trans);

		// update and publish odom message
		odom.header.stamp = current_time;
	   	odom.pose.pose.position.x = x;
       	odom.pose.pose.position.y = y;
	    odom.pose.pose.orientation = odom_quat;

	    odom_pub.publish(odom);

        last_time = current_time;
	   	r.sleep();

	}


}
