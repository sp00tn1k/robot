#!/usr/bin/env python
import rospy
import numpy as np
import math
from copy import deepcopy

from sensor_msgs.msg import LaserScan, Image
from geometry_msgs.msg import Twist, PoseStamped, Pose, Point, Quaternion
from std_msgs.msg import Int8, Int8MultiArray
from dynamic_reconfigure.server import Server
from assisted_teleop.cfg import AssistedTeleopConfig
import tf2_ros

# TODOS
# assist level 2: Adjust angular velocity
# Compute situational reduced and slow distance from current velocity
# Change proximities to work with laser messages covering different angles (e.g., 180, 270, 360)
# self.obstacles: Represent as array
# Handle depth images

class AssistedTeleop:
    def __init__(self):

        """ Parameters """
        self.robot_radius = rospy.get_param('~robot_radius', 0.30)
        self.base_frame = rospy.get_param('~base_frame', 'base_footprint')
        self.laser_frame = rospy.get_param('~laser_frame', 'laser')
        self.reduced_vel = rospy.get_param('~reduced_vel', 0.25)
        self.slow_vel = rospy.get_param('~slow_vel', 0.05)
        self.reduced_range = self.robot_radius + rospy.get_param('~reduced_range', 0.4)
        self.slow_range = self.robot_radius + rospy.get_param('~slow_range', 0.15)
        self.check_range = self.robot_radius + rospy.get_param('~check_range', 0.7)
        self.skip_points = rospy.get_param('~skip_points', '0')
        self.use_depth = rospy.get_param('~use_depth', False) # TODO
        self.use_velocity = rospy.get_param('~use_velocity', False) # TODO
        self.pub_proximities = rospy.get_param('~pub_proximities', True)

        """ Publishers and subscribers """
        self.cmd_vel_out_pub = rospy.Publisher("cmd_vel", Twist, queue_size=10)
        self.assist_actual_pub = rospy.Publisher("assist_actual", Int8, queue_size=10)
        self.proximities_pub = rospy.Publisher("proximities", Int8MultiArray, queue_size=10)
        self.proximities_msg = Int8MultiArray()

        rospy.Subscriber("scan", LaserScan, self.laser_callback)
        rospy.Subscriber("cmd_vel_in", Twist, self.twist_callback)
        rospy.Subscriber("assist_level", Int8, self.assist_level_cb)


        self.frontal_angle = 0    # Angle in radians corresponding to straight ahead
        self.assist_level = 0     # The maximum enabled assist level [0, 1, 2]
        self.assist_actual = 0    # The currently active assist level ( <= assist_level )

        self.tf_buffer = tf2_ros.Buffer()
        self.tf_listener = tf2_ros.TransformListener(self.tf_buffer)

        self.laser_pos = None
        self.laser_orient = None

        self.lfr = [0] * 3     # obstacle left, front, right {0, 1, 2}

        # assuming everything [-pi; -pi/2] as to the front right
        # [-pi/2;pi/2] front

        """ Find position of laser relative to base frame"""
        while self.laser_pos is None and not rospy.is_shutdown():
            try:
                self.laser_trans = self.tf_buffer.lookup_transform(self.base_frame, self.laser_frame, rospy.Time(0))
                rospy.loginfo("Transform between frames %s and %s found: %s, %s", self.base_frame, self.laser_frame, self.laser_trans.transform.translation.x, self.laser_trans.transform.translation.y)
                break
            except (tf2_ros.LookupException, tf2_ros.ConnectivityException, tf2_ros.ExtrapolationException):
                rospy.logwarn("Transform between frames %s and %s not found.", self.base_frame, self.laser_frame)
                rospy.sleep(1)
                continue

        self.last_laser_time = rospy.Time.now()
        self.last_laser = None
        self.last_nonzero_twist = rospy.Time.now().to_sec()


        if self.use_depth:
            rospy.Subscriber("depth_image", Image, self.depth_callback)

        if self.use_velocity:
            rospy.Subscriber("velocity", Twist, self.vel_callback)
            self.actual_vel = [0,0]


    def laser_callback(self, data):
        """Process incoming laser data using bins for angle intervals"""
        if self.assist_level or self.pub_proximities:
            rospy.logdebug("Laser Callback")
            self.last_laser = data
            self.last_laser_time = rospy.Time.now()
            self.closest_range_front = 100.0
            self.closest_range_left = 100.0
            self.closest_range_right = 100.0
            closest_range_angle = 0
            self.obstacle = 0 # start by assuming the path is clear
            bin_size = math.floor(len(data.ranges) / 8)
            distances = [0] * len(data.ranges)
            self.lfr = [0] * 3     # obstacle left, front, right {0, 1, 2}


            # identify obstacles: front, left, right, by range: below reduced_range, slow_range
            for i in range(0, len(data.ranges)):
                # only check scan points below a certain threshold
                if data.ranges[i] > data.range_min and data.ranges[i] < self.check_range:
                    # compute scan point position relative to base frame
                    range_x = math.cos(data.angle_min + i*data.angle_increment) * data.ranges[i] + self.laser_trans.transform.translation.x
                    range_y = math.sin(data.angle_min + i*data.angle_increment) * data.ranges[i] + self.laser_trans.transform.translation.y
                    #print((data.angle_min+i*data.angle_increment), data.ranges[i], range_x, range_y)
                    # scan point distance to base frame
                    distances[i] = math.sqrt(range_x*range_x + range_y*range_y)

                    # right bucket
                    if range_y > (self.robot_radius + 0.01) and (distances[i] < self.closest_range_right):
                        self.closest_range_right = distances[i]
                    # front bucket 
                    elif (abs(range_y) < self.robot_radius + 0.01) and (distances[i] < self.closest_range_front):
                        self.closest_range_front = distances[i]
                    # left bucket
                    elif range_y < (-self.robot_radius - 0.01) and (distances[i] < self.closest_range_left):
                        self.closest_range_left = distances[i]
                    #if distances[i] < closest_range:
                    #    closest_range = distances[i] - self.robot_radius
                    #    closest_range_angle = i * data.angle_increment 


                else:
                    distances[i] = 100
                    
            #rospy.loginfo("Closest distances on left, front, right: %s, %s, %s", self.closest_range_left, self.closest_range_front, self.closest_range_right)
            if self.closest_range_front < self.reduced_range:
                self.obstacle = 1
                self.lfr[1] = 1
            if self.closest_range_left < self.slow_range:
                self.obstacle = 2
                self.lfr[0] = 1
            if self.closest_range_right < self.slow_range:
                self.obstacle = 3
                self.lfr[2] = 1
            if self.closest_range_front < self.slow_range:
                self.obstacle = 4
                self.lfr[1] = 2
            rospy.logdebug("obstacle: %s", self.obstacle)
                   
            if self.pub_proximities:
                """ Publish array of proximities by direction - 0: free, 1: far, 2: close, 3: below laser's minimum range """
                self.proximities = [0] * 8
                for i in range(0, 8):
                    for j in range(0, bin_size):
                        if data.ranges[i*bin_size + j] < 0.13:
                            self.proximities[i] = 3
                            break
                        if distances[i*bin_size + j] < self.reduced_range:
                            if distances[i*bin_size + j] > self.slow_range:
                                self.proximities[i] = 1
                            else:
                                self.proximities[i] = 2
                                break
                self.proximities_msg.data = self.proximities
                self.proximities_pub.publish(self.proximities_msg)


    def twist_callback(self, data):
        """Process incoming twist message and publish modified message if assist is active and obstacles recognized"""
        #if not self.last_laser is None and (rospy.Time.now().to_sec() - self.last_laser_time.to_sec()) < 2:
        rospy.logdebug("Twist Callback")
        since_last_laser = rospy.Time.now().to_sec() - self.last_laser_time.to_sec()
        #since_last_nonzero = rospy.Time.now().to_sec() - self.last_laser_time.to_sec()
        if (data.linear.x != 0.0 or data.angular.z != 0.0):
            self.last_nonzero_twist = rospy.Time.now().to_sec()

        #if rospy.Time.now().to_sec() - self.last_nonzero_twist > 1.0:
        #    break
        self.assist_actual = 0
        if not self.assist_level:
            pass
        elif self.last_laser is None or since_last_laser > 2:
            rospy.logwarn("No laser message received in %s seconds. Passing original message through unfiltered.", since_last_laser)

        else:
            #if self.obstacle ==  0 or data.linear.x == 0.0:
            if sum(self.lfr) == 0 or data.linear.x == 0.0:
                pass
            else:
                data.linear.x = min(data.linear.x, self.reduced_vel)                
                self.assist_actual = 1
                if self.lfr[1] == 2:
                    data.linear.x = min(data.linear.x, self.slow_vel) 
                    
                if self.assist_level == 2:
                    if self.lfr[0] == 1 and self.lfr[2] == 0:
                        if data.angular.z <= 0.0:
                            data.angular.z = min(data.angular.z, -0.6)
                            self.assist_actual = 2
                    elif self.lfr[0] == 0 and self.lfr[2] == 1:
                        if data.angular.z >= 0.0:
                            data.angular.z = max(data.angular.z, 0.6)
                            self.assist_actual = 2
                    
            #elif self.obstacle ==  1: # front slow distance
            #    rospy.loginfo("Obstacle near ahead: Slowing down to %s", self.slow_vel)
            #elif self.obstacle == 2: # left slow distance
            #    rospy.loginfo("Obstacle on the left: Slowing down to %s", self.reduced_vel)
            #elif self.obstacle == 3: # right slow distance
            #    rospy.loginfo("Obstacle on the right: Slowing down to %s", self.reduced_vel)
            #elif self.obstacle == 4: # front reduced distance
            #    rospy.loginfo("Obstacle ahead: Slowing down to %s", self.reduced_vel)

            # attempt to make keeping straight easier
            #if data.linear.x > self.slow_vel:
            #    data.angular.z = 0.25 * data.angular.z


            #if data.linear.x > self.slow_vel and abs(data.angular.z) < 0.2:
            #    data.angular.z = 0.0 
            #elif data.linear.x > self.slow_vel and abs(data.angular.z) > 0.2:
            #    data.angular.z = 0.5 * data.angular.z

        rospy.logdebug("Output linear: %s, angular: %s", data.linear.x)
        if rospy.Time.now().to_sec() - self.last_nonzero_twist < 2.5:
            self.cmd_vel_out_pub.publish(data)
            self.assist_actual_pub.publish(self.assist_actual)

    def depth_callback(self, data):
        """TODO: Use depth data to check for obstacles"""
        pass

    def vel_callback(self, data):
        """TODO: Use current velocity to calculate appropriate twist output"""
        self.actual_vel = [data.linear.x, data.angular.z]
        pass

    def assist_level_cb(self, data):
        """Set assist level"""
        self.assist_level = data.data


if __name__ == '__main__':
    try:                                                                                                                               
        rospy.init_node('assisted_teleop')
        at = AssistedTeleop()
        rospy.spin()
    
    except rospy.ROSInterruptException:
        pass

