#!/usr/bin/env python
import rospy

from std_msgs.msg import Bool

class UVDisinfection:
  def __init__(self):
    
    # Time in seconds of no incoming messages before lamps are switched off 
    self.connection_timeout = rospy.get_param('~timeout', 10) 

    rospy.Subscriber("uv_active", Bool, self.active_cb)

    self.last_msg_time = rospy.Time.now()
    self.last_msg = False
    self.lamps_on = False

  def active_cb(self, data):
    self.last_msg = data.data
    self.last_msg_time = rospy.Time.now()

  def loop(self):
    while not rospy.is_shutdown():
      if ((rospy.Time.now() - self.last_msg_time).to_sec() < self.connection_timeout) and self.last_msg:
        self.lamps_on = True
        rospy.loginfo("UV active.")
      else:
        self.lamps_on = False
        rospy.loginfo("UV inactive.")
      rospy.sleep(1)                
        
if __name__ == '__main__':
  try:                                                                                                                               
    rospy.init_node('uv_disinfection')
    uvd = UVDisinfection()
    uvd.loop()

  except rospy.ROSInterruptException:
    pass

