# sp00tn1k robot: an open-source platform for telepresence research

**[Robot installation](#installation)** | **[Base]** | **[UI]** | **[Client]** | **[Hardware]** | **[Peer Server]**

The robot is based on ROS, making use of standard ROS packages and some custom-de...
The repository works in conjunction with several other repositories:

* The [robot's user interface][UI]
* The [user client][Client]
* The [base][Base]
* Hardware components and assembly manuals are located in the [hardware repository][Hardware]
* The [peer server][Peer Server]


A discussion of the platform in more detail can be found [here][Paper]

Installation instructions for the other components can be found in the respective repositories.

---
## Installation

This installation guide was tested on Ubuntu 20.04 and assumes that ROS noetic is installed (http://wiki.ros.org/noetic/Installation/Ubuntu).

The components listed below are partially optional and marked accordingly.


Some essential and utility tools
```bash
sudo apt install python3-catkin-tools python3-osrf-pycommon python3-vcstool python3-rosdep ros-noetic-navigation
```

### Create the ROS workspace with catkin tools, vcstool and rosdep
```bash
cd && mkdir -p sp00tn1k_ros/src

cd sp00tn1k_ros && catkin init && catkin build

echo "source ~/sp00tn1k_ros/devel/setup.bash" >> ~/.bashrc

echo "export ROS_WORKSPACE=~/sp00tn1k_ros" >> ~/.bashrc

cd src

git clone https://bitbucket.org/sp00tn1k/robot.git -b develop

roscd && vcs import src < src/.rosinstall

roscd && sudo rosdep init && rosdep update

rosdep install --from-paths src --ignore-src -r -y

```


[UI]: https://bitbucket.org/sp00tn1k/ui
[Client]: https://bitbucket.org/sp00tn1k/client
[Base]: https://bitbucket.org/sp00tn1k/base
[Hardware]: https://bitbucket.org/sp00tn1k/hardware
[Peer Server]: https://bitbucket.org/sp00tn1k/peer-server
[Paper]: https://arxiv.org/pdf/2102.01551.pdf
[Picture]: https://bitbucket.org/sp00tn1k/robot/src/develop/docs/images/sp00tn1k-infrastructure.jpg